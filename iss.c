#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define TAM 65536 /* Constante que define o tamanho das memorias */

FILE *in;

unsigned short int MEM_INST[TAM]; /* Implementa a memoria de instrucoes*/
unsigned char MEM_DATA[TAM]; /*Implementa a memoria de dados */

unsigned short int mem_inst,mem_data; /* Contador do numero de instrucoes e de dados */
int ciclos,end_mem,ini_inter,fim_inter,contador2,flag=0,tempo;

short int BD[31]; /* Implementa o banco de registradores */
unsigned short int PC; /* Implementa o contador de programa */

unsigned short int instrucao0;
unsigned short int instrucao1;
unsigned short int instrucao2;
unsigned short int instrucao3;

unsigned short int temp0,temp1,temp2,x,y,z;
char sinstrucao[40],temp[20],c;
struct instrucao
{
	unsigned short int opcode;
	unsigned short int formato;
	short int desl;
	unsigned short int reg1;
	unsigned short int reg2;
	unsigned short int reg3;
	unsigned short int reg4;
	short int cost;
	unsigned short int regdest;
	unsigned short int modo;
}inst0,inst1,t;

unsigned short int tinstrucao0,tinstrucao1,tinstrucao2,tinstrucao3;
short int overflow,zero,carry;
int contador,flag2;
char opt,opt1;

void registrador(struct instrucao x)
{
		if(x.reg1 == 0)strcat(sinstrucao,",$r0");
		if(x.reg1 == 1)strcat(sinstrucao,",$r1");
		if(x.reg1 == 2)strcat(sinstrucao,",$r2");
		if(x.reg1 == 3)strcat(sinstrucao,",$r3");
		if(x.reg1 == 4)strcat(sinstrucao,",$r4");
		if(x.reg1 == 5)strcat(sinstrucao,",$r5");
		if(x.reg1 == 6)strcat(sinstrucao,",$r6");
		if(x.reg1 == 7)strcat(sinstrucao,",$r7");
		if(x.reg1 == 8)strcat(sinstrucao,",$r8");
		if(x.reg1 == 9)strcat(sinstrucao,",$r9");
		if(x.reg1 == 10)strcat(sinstrucao,",$r10");
		if(x.reg1 == 11)strcat(sinstrucao,",$r11");
		if(x.reg1 == 12)strcat(sinstrucao,",$r12");
		if(x.reg1 == 13)strcat(sinstrucao,",$r13");
		if(x.reg1 == 14)strcat(sinstrucao,",$r14");
		if(x.reg1 == 15)strcat(sinstrucao,",$r15");
		if(x.reg1 == 16)strcat(sinstrucao,",$r16");
		if(x.reg1 == 17)strcat(sinstrucao,",$r17");
		if(x.reg1 == 18)strcat(sinstrucao,",$r18");
		if(x.reg1 == 19)strcat(sinstrucao,",$r19");
		if(x.reg1 == 20)strcat(sinstrucao,",$r20");
		if(x.reg1 == 21)strcat(sinstrucao,",$r21");
		if(x.reg1 == 22)strcat(sinstrucao,"$r22");
		if(x.reg1 == 23)strcat(sinstrucao,"$r23");
		if(x.reg1 == 24)strcat(sinstrucao,"$r24");
		if(x.reg1 == 25)strcat(sinstrucao,"$r25");
		if(x.reg1 == 26)strcat(sinstrucao,"$r26");
		if(x.reg1 == 27)strcat(sinstrucao,"$r27");
		if(x.reg1 == 28)strcat(sinstrucao,",$t1")
		if(x.reg1 == 29)strcat(sinstrucao,",$t2");
		if(x.reg1 == 30)strcat(sinstrucao,",$t3");	
		if(x.reg1 == 31)strcat(sinstrucao,",$t4");
	if(x.opcode == 24 || x.opcode == 25 || x.opcode == 29 ||){}
	else
	{
		if(x.reg2 == 0)strcat(sinstrucao,",$r0");
		if(x.reg2 == 1)strcat(sinstrucao,",$r1");
		if(x.reg2 == 2)strcat(sinstrucao,",$r2");
		if(x.reg2 == 3)strcat(sinstrucao,",$r3");
		if(x.reg2 == 4)strcat(sinstrucao,",$r4");
		if(x.reg2 == 5)strcat(sinstrucao,",$r5");
		if(x.reg2 == 6)strcat(sinstrucao,",$r6");
		if(x.reg2 == 7)strcat(sinstrucao,",$r7");
		if(x.reg2 == 8)strcat(sinstrucao,",$r8");
		if(x.reg2 == 9)strcat(sinstrucao,",$r9");
		if(x.reg2 == 10)strcat(sinstrucao,",$r10");
		if(x.reg2 == 11)strcat(sinstrucao,",$r11");
		if(x.reg2 == 12)strcat(sinstrucao,",$r12");
		if(x.reg2 == 13)strcat(sinstrucao,",$r13");
		if(x.reg2 == 14)strcat(sinstrucao,",$r14");
		if(x.reg2 == 15)strcat(sinstrucao,",$r15");
		if(x.reg2 == 16)strcat(sinstrucao,",$r16");
		if(x.reg2 == 17)strcat(sinstrucao,",$r17");
		if(x.reg2 == 18)strcat(sinstrucao,",$r18");
		if(x.reg2 == 19)strcat(sinstrucao,",$r19");
		if(x.reg2 == 20)strcat(sinstrucao,",$r20");
		if(x.reg2 == 21)strcat(sinstrucao,",$r21");
		if(x.reg2 == 22)strcat(sinstrucao,"$r22");
		if(x.reg2 == 23)strcat(sinstrucao,"$r23");
		if(x.reg2 == 24)strcat(sinstrucao,"$r24");
		if(x.reg2 == 25)strcat(sinstrucao,"$r25");
		if(x.reg2 == 26)strcat(sinstrucao,"$r26");
		if(x.reg2 == 27)strcat(sinstrucao,"$r27");
		if(x.reg2 == 28)strcat(sinstrucao,",$t1")
		if(x.reg2 == 29)strcat(sinstrucao,",$t2");
		if(x.reg2 == 30)strcat(sinstrucao,",$t3");	
		if(x.reg2 == 31)strcat(sinstrucao,",$t4");		
	}
	if(x.opcode == 24 || x.opcode == 25 || x.opcode == 29 || x.opcode == 0 || x.opcode == 3 || x.opcode == 6 || x.opcode == 9 || x.opcode == 12 || x.opcode == 18 || x.opcode == 21 || x.opcode == 26 || x.opcode == 27 || x.opcode == 28 || x.opcode == 30){}
	else
	{
		if(x.reg3 == 0)strcat(sinstrucao,",$r0");
		if(x.reg3 == 1)strcat(sinstrucao,",$r1");
		if(x.reg3 == 2)strcat(sinstrucao,",$r2");
		if(x.reg3 == 3)strcat(sinstrucao,",$r3");
		if(x.reg3 == 4)strcat(sinstrucao,",$r4");
		if(x.reg3 == 5)strcat(sinstrucao,",$r5");
		if(x.reg3 == 6)strcat(sinstrucao,",$r6");
		if(x.reg3 == 7)strcat(sinstrucao,",$r7");
		if(x.reg3 == 8)strcat(sinstrucao,",$r8");
		if(x.reg3 == 9)strcat(sinstrucao,",$r9");
		if(x.reg3 == 10)strcat(sinstrucao,",$r10");
		if(x.reg3 == 11)strcat(sinstrucao,",$r11");
		if(x.reg3 == 12)strcat(sinstrucao,",$r12");
		if(x.reg3 == 13)strcat(sinstrucao,",$r13");
		if(x.reg3 == 14)strcat(sinstrucao,",$r14");
		if(x.reg3 == 15)strcat(sinstrucao,",$r15");
		if(x.reg3 == 16)strcat(sinstrucao,",$r16");
		if(x.reg3 == 17)strcat(sinstrucao,",$r17");
		if(x.reg3 == 18)strcat(sinstrucao,",$r18");
		if(x.reg3 == 19)strcat(sinstrucao,",$r19");
		if(x.reg3 == 20)strcat(sinstrucao,",$r20");
		if(x.reg3 == 21)strcat(sinstrucao,",$r21");
		if(x.reg3 == 22)strcat(sinstrucao,"$r22");
		if(x.reg3 == 23)strcat(sinstrucao,"$r23");
		if(x.reg3 == 24)strcat(sinstrucao,"$r24");
		if(x.reg3 == 25)strcat(sinstrucao,"$r25");
		if(x.reg3 == 26)strcat(sinstrucao,"$r26");
		if(x.reg3 == 27)strcat(sinstrucao,"$r27");
		if(x.reg3 == 28)strcat(sinstrucao,",$t1")
		if(x.reg3 == 29)strcat(sinstrucao,",$t2");
		if(x.reg3 == 30)strcat(sinstrucao,",$t3");	
		if(x.reg3 == 31)strcat(sinstrucao,",$t4");		
	if(x.opcode == 24 || x.opcode == 25 || x.opcode == 29 || x.opcode == 0 || x.opcode == 3 || x.opcode == 6 || x.opcode == 9 || x.opcode == 12 || x.opcode == 18 || x.opcode == 21 || x.opcode == 26 || x.opcode == 27 || x.opcode == 28 || x.opcode == 30 || x.opcode == 1 || x.opcode == 3 || x.opcode == 7 || x.opcode == 10 || x.opcode == 13 || x.opcode == 16 || x.opcode == 19 || x.opcode == 23){}
	else
	{
		if(x.reg4 == 0)strcat(sinstrucao,",$r0");
		if(x.reg4 == 1)strcat(sinstrucao,",$r1");
		if(x.reg4 == 2)strcat(sinstrucao,",$r2");
		if(x.reg4 == 3)strcat(sinstrucao,",$r3");
		if(x.reg4 == 4)strcat(sinstrucao,",$r4");
		if(x.reg4 == 5)strcat(sinstrucao,",$r5");
		if(x.reg4 == 6)strcat(sinstrucao,",$r6");
		if(x.reg4 == 7)strcat(sinstrucao,",$r7");
		if(x.reg4 == 8)strcat(sinstrucao,",$r8");
		if(x.reg4 == 9)strcat(sinstrucao,",$r9");
		if(x.reg4 == 10)strcat(sinstrucao,",$r10");
		if(x.reg4 == 11)strcat(sinstrucao,",$r11");
		if(x.reg4 == 12)strcat(sinstrucao,",$r12");
		if(x.reg4 == 13)strcat(sinstrucao,",$r13");
		if(x.reg4 == 14)strcat(sinstrucao,",$r14");
		if(x.reg4 == 15)strcat(sinstrucao,",$r15");
		if(x.reg4 == 16)strcat(sinstrucao,",$r16");
		if(x.reg4 == 17)strcat(sinstrucao,",$r17");
		if(x.reg4 == 18)strcat(sinstrucao,",$r18");
		if(x.reg4 == 19)strcat(sinstrucao,",$r19");
		if(x.reg4 == 20)strcat(sinstrucao,",$r20");
		if(x.reg4 == 21)strcat(sinstrucao,",$r21");
		if(x.reg4 == 22)strcat(sinstrucao,"$r22");
		if(x.reg4 == 23)strcat(sinstrucao,"$r23");
		if(x.reg4 == 24)strcat(sinstrucao,"$r24");
		if(x.reg4 == 25)strcat(sinstrucao,"$r25");
		if(x.reg4 == 26)strcat(sinstrucao,"$r26");
		if(x.reg4 == 27)strcat(sinstrucao,"$r27");
		if(x.reg4 == 28)strcat(sinstrucao,",$t1")
		if(x.reg4 == 29)strcat(sinstrucao,",$t2");
		if(x.reg4 == 30)strcat(sinstrucao,",$t3");	
		if(x.reg4 == 31)strcat(sinstrucao,",$t4");		
	}
}
int retorna_instrucao(struct instrucao t)
{
	sinstrucao[0] = '\0';
	if(t.opcode == 57)strcpy(sinstrucao,"nop");
	else if(t.opcode == 0)strcpy(sinstrucao,"add2 ");
	else if(t.opcode == 1)strcpy(sinstrucao,"add3 ");
	else if(t.opcode == 2)strcpy(sinstrucao,"add4 ");
	else if(t.opcode == 3)strcpy(sinstrucao,"fadd2 ");
	else if(t.opcode == 4)strcpy(sinstrucao,"fadd3 ");
	else if(t.opcode == 5)strcpy(sinstrucao,"fadd4 ");
	else if(t.opcode == 6)strcpy(sinstrucao,"sub2 ");
	else if(t.opcode == 7)strcpy(sinstrucao,"sub3 ");
	else if(t.opcode == 8)strcpy(sinstrucao,"sub4 ");
	else if(t.opcode == 9)strcpy(sinstrucao,"fsub2 ");
	else if(t.opcode == 10)strcpy(sinstrucao,"fsub3 ");
	else if(t.opcode == 11)strcpy(sinstrucao,"fsub4 ");
	else if(t.opcode == 12)strcpy(sinstrucao,"mul2 ");
	else if(t.opcode == 13)strcpy(sinstrucao,"mul3 ");
	else if(t.opcode == 14)strcpy(sinstrucao,"mul4 ");
	else if(t.opcode == 15)strcpy(sinstrucao,"fmul2 ");
	else if(t.opcode == 16)strcpy(sinstrucao,"fmul3 ");
	else if(t.opcode == 17)strcpy(sinstrucao,"fmul4 ");
	else if(t.opcode == 18)strcpy(sinstrucao,"div2 ");
	else if(t.opcode == 19)strcpy(sinstrucao,"div3 ");
	else if(t.opcode == 20)strcpy(sinstrucao,"div4 ");
	else if(t.opcode == 21)strcpy(sinstrucao,"fdiv2 ");
	else if(t.opcode == 22)strcpy(sinstrucao,"fdiv3 ");
	else if(t.opcode == 23)strcpy(sinstrucao,"fdiv4 ");	
	else if(t.opcode == 24)strcpy(sinstrucao,"inc1 ");
	else if(t.opcode == 25)strcpy(sinstrucao,"dec1 ");
	else if(t.opcode == 26)strcpy(sinstrucao,"mod2 ");
	else if(t.opcode == 27)strcpy(sinstrucao,"and2 ");
	else if(t.opcode == 28)strcpy(sinstrucao,"or2 ");	
	else if(t.opcode == 29)strcpy(sinstrucao,"not1 ");	
	else if(t.opcode == 30)strcpy(sinstrucao,"xor2 ");
	else if(t.opcode == 31)strcpy(sinstrucao,"shr2 ");
	else if(t.opcode == 32)strcpy(sinstrucao,"shl2 ");
	else if(t.opcode == 33)strcpy(sinstrucao,"lb1 ");
	else if(t.opcode == 34)strcpy(sinstrucao,"lb2 ");
	else if(t.opcode == 35)strcpy(sinstrucao,"lb3 ");
	else if(t.opcode == 36)strcpy(sinstrucao,"lb4 ");	
	else if(t.opcode == 37)strcpy(sinstrucao,"sw1 ");
	else if(t.opcode == 38)strcpy(sinstrucao,"sw2 ");
	else if(t.opcode == 39)strcpy(sinstrucao,"sw3 ");
	else if(t.opcode == 40)strcpy(sinstrucao,"sw4 ");
	else if(t.opcode == 41)strcpy(sinstrucao,"sb1 ");
	else if(t.opcode == 42)strcpy(sinstrucao,"sb2 ");
	else if(t.opcode == 43)strcpy(sinstrucao,"sb3 ");
	else if(t.opcode == 44)strcpy(sinstrucao,"sb4 ");	
	else if(t.opcode == 45)strcpy(sinstrucao,"lw1 ");
	else if(t.opcode == 46)strcpy(sinstrucao,"lw2 ");
	else if(t.opcode == 47)strcpy(sinstrucao,"lw3 ");
	else if(t.opcode == 48)strcpy(sinstrucao,"lw4 ");
	else if(t.opcode == 49)strcpy(sinstrucao,"cp1 ");
	else if(t.opcode == 50)strcpy(sinstrucao,"cp2 ");
	else if(t.opcode == 51)strcpy(sinstrucao,"cp3 ");
	else if(t.opcode == 52)strcpy(sinstrucao,"cb ");
	else if(t.opcode == 53)strcpy(sinstrucao,"beq ");
	else if(t.opcode == 54)strcpy(sinstrucao,"bne ");
	else if(t.opcode == 55)strcpy(sinstrucao,"jpc ");
	else if(t.opcode == 56)strcpy(sinstrucao,"jpi ");
	else if(t.opcode == 58)
	{
		strcpy(sinstrucao,"halt");
		return(0);
	}else
	{
		strcpy(sinstrucao,"    ");
		return(0);
	}
	if(t.formato == 0)
	{
			if(t.opcode == 0 || t.opcode == 30){}
			else if(t.modo == 2)
			{
				strcat(sinstrucao,"00,");
				sprintf(temp,"%d",t.regdest);
				strcat(sinstrucao,temp);
				strcat(sinstrucao,",");
				registrador(t);
			}
		}	
	}
	else if(t.formato == 1)
	{
		sprintf(temp,"%d",t.regdest);
		strcat(sinstrucao,temp);
		strcat(sinstrucao,",");
		sprintf(temp,"%d",t.cost);
		strcat(sinstrucao,temp);
	}
	else if(t.formato == 2)
	{
		registrador(t);
	}
	else if(t.formato == 3)
	{
		sprintf(temp,"%d",t.desl);
		strcat(sinstrucao,temp);
	}
}
void TABELA_DE_ERROS(int erro) /* Mostra os erros gerados pelo montador */
{
	switch(erro)
	{
		case(0):
		{
			printf("Erro(%d) : O arquivo .bin nao foi especificado !!\n",erro+1);
			break;
		}
		case(1):
		{
			printf("Erro(%d) : Arquivo nao encontrado !!\n",erro+1);
			break;
		}
	}
	exit(0);
}
void ARQUIVO_ENTRADA(int parametro,char nome_arquivo[30]) /* Responsavel por abrir o arquivo de entrada */
{
	if(parametro <= 1) TABELA_DE_ERROS(0);
	in = fopen(nome_arquivo,"rb");
	if(in == NULL) TABELA_DE_ERROS(1);
	contador = strlen(nome_arquivo);
	if(!(nome_arquivo[contador-3] == 'b' && nome_arquivo[contador-2] == 'i' && nome_arquivo[contador-1] == 'n'))TABELA_DE_ERROS(1);
}
void LE_MEMORIA_INSTRUCAO() /* Funcao responsavel por iniciar a memoria de instrucoes */
{
	fread(&mem_inst,sizeof(unsigned short int),1,in); /* Le o numero de instrucoes no .bin */
	fread(&mem_data,sizeof(unsigned short int),1,in); /* Le o numero de dados no .bin */
	for(contador=0;contador<mem_inst;contador++)
	{
		fread(&MEM_INST[contador],sizeof(unsigned short int),1,in); /* Le as instrucoes */
	}
}
void LE_MEMORIA_DADOS() /* Funcao responsavel por iniciar a memoria de dados */
{
	for(contador=0;contador<mem_data;contador++)
	{
		fread(&MEM_DATA[contador],sizeof(char),1,in); /* Le os dados */
	}
}
void INICIA_SIMULADOR() /* Funcao responsavel por iniciar o simulador */
{
	PC = 0; /* Inicia o contador de programa */
	for(contador=0;contador<32;contador++){BD[contador] =0;} /* inicia o banco de registradores */
	for(contador=0;contador<TAM;contador++)
	{
		MEM_INST[contador] = -1;
		MEM_DATA[contador] = -1;
	}
	flag2=0;
	zero=-1,carry=-1,overflow=-1;
}
int BUSCA_INSTRUCAO() /* Funcao responsavel por buscar as instrucoes */
{	
	instrucao0 = MEM_INST[PC]; /* Busca a primeira molecula (Instrucao 0 + Instrucao 1) */
	if(instrucao0 == 61440)
	{
		return(1);
	}
	PC++;
	return(0);
}
struct instrucao Decodifica(unsigned short int instrucao0)
{
	tinstrucao0 = instrucao0;
	tinstrucao0 = tinstrucao0 >> 11;
	tinstrucao0 = tinstrucao0 | 0;
	t.opcode = tinstrucao0;
	if(t.opcode >= 0 && t.opcode <= 32) /* Trata instrucao do formato LA - 6 Campos */
	{
		t.formato = 0; /* Pega o formato da instrucao */
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 5;
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 >> 14; /* Pega o modo de enderecamento da instrucao 0 */
		tinstrucao0 = tinstrucao0 | 0;
		t.modo = 2;
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 7;
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 >> 15; /* Pega o registrador destino da instrucao 0 */
		tinstrucao0 = tinstrucao0 | 0;
		t.regdest = tinstrucao0;
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 8;
		tinstrucao0 = tinstrucao0 | 0; 
		tinstrucao0 = tinstrucao0 >> 12; /* Pega o operando 0 da instrucao */
		tinstrucao0 = tinstrucao0 | 0;
		t.reg1 = tinstrucao0;
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 12; 
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 >> 12; /* Pega o operando 1 da instrucao */
		tinstrucao0 = tinstrucao0 | 0;
		t.reg2 = tinstrucao0;
	}
	if(t.opcode >= 52 && t.opcode <= 53) /* Trata instrucao do formato I - 3 Campos */
	{
		t.formato = 1; /* Pega o formato da instrucao */
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 7;
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 >> 15; /* Pega o registrador destino da instrucao */
		tinstrucao0 = tinstrucao0 | 0;
		t.regdest = tinstrucao0;
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 8; /* Pega a constante da instrucao */
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 >> 8; 
		tinstrucao1 = tinstrucao0 >> 7;
		if(tinstrucao1 == 1)
		{
			tinstrucao1 = tinstrucao0;
			tinstrucao1 = ~(tinstrucao1); //0000 0000 1111 1110           1111 1111 0000 0001
			tinstrucao1 = tinstrucao1 << 8;
			tinstrucao1 = tinstrucao1 >> 8;
			tinstrucao1 = tinstrucao1 + 1;
			tinstrucao1 = tinstrucao1 << 8;
			tinstrucao1 = tinstrucao1 >> 8;
			tinstrucao0 = tinstrucao1;
			t.cost = tinstrucao0;
			t.cost = t.cost*-1;
		}
		else
		{
			t.cost = tinstrucao0;
		}
	}
	if(t.opcode >= 33 && t.opcode <= 51) /* Trata instrucao do formato Load/Store - 6 Campos */
	{
		t.formato = 2; /* Pega o formato da instrucao */
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 8;
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 >> 12;
		tinstrucao0 = tinstrucao0 | 0;
		t.reg1 = tinstrucao0;
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 12;
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 >> 12; /* Pega o operando 1 da instrucao */
		tinstrucao0 = tinstrucao0 | 0;
		t.reg2 = tinstrucao0;
	}
	if(t.opcode >= 23 && t.opcode <= 29) /* Trata instrucao do formato Branch - 2 Campos */
	{
		t.formato = 3; /* Pega o formato da instrucao */
		tinstrucao0 = instrucao0;
		tinstrucao0 = tinstrucao0 << 5;
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao0 = tinstrucao0 >> 5; /* Pega o deslocamento da instrucao */
		tinstrucao0 = tinstrucao0 | 0;
		tinstrucao1 = tinstrucao0 >> 10;
		if(tinstrucao1 == 1)
		{
			
			tinstrucao1 = tinstrucao0;
			tinstrucao1 = ~(tinstrucao1); //0000 0000 1111 1110           1111 1111 0000 0001
			tinstrucao1 = tinstrucao1 << 5;
			tinstrucao1 = tinstrucao1 >> 5;
			tinstrucao1 = tinstrucao1 + 1;
			tinstrucao1 = tinstrucao1 << 5;
			tinstrucao1 = tinstrucao1 >> 5;
			tinstrucao0 = tinstrucao1;
			t.desl = tinstrucao0;
			t.desl = t.desl*-1;
		}
		else
		{
			t.desl = tinstrucao0;
		}
		}
	return(t);
}
int DECODIFICA_INSTRUCAO() /* Funcao responsavel por decodificar as instrucoes lidas */
{
	inst0 = Decodifica(instrucao0);
}

void Ula(struct instrucao t,int tipo) /* Funcao que implementa a unidade logica 0 */
{
	// Bloco para tratar de carry e overflow
	if(t.opcode == 0 || t.opcode == 3 || t.opcode == 6 || t.opcode == 9 || t.opcode == 12 || t.opcode == 15 || t.opcode == 18 || t.opcode == 21) // R
	{
		if(t.modo == 2)
		{
			if(BD[t.reg1] >= 0 && BD[t.reg2] >= 0 && (BD[t.reg1] + BD[t.reg2]) < 0) overflow = 1;
			else if(BD[t.reg1] < 0 && BD[t.reg2] < 0 && (BD[t.reg1] + BD[t.reg2]) >= 0)overflow =1;
			else if(BD[t.reg1] < 0 && BD[t.reg2] >= 0 && (BD[t.reg1] -BD[t.reg2]) < 0)overflow = 1;
			else if(BD[t.reg1] >= 0 && BD[t.reg2] < 0 && (BD[t.reg1] - BD[t.reg2]) >= 0)overflow =1;
			else overflow=0;
			if(overflow == 1)carry=1;
			else carry=0;
		}
		
		if(t.opcode == 1 || t.opcode == 4 || t.opcode == 7 || t.opcode == 10 || t.opcode == 13 || t.opcode == 16 || t.opcode == 19 || t.opcode == 22) // R
	{
		if(t.modo == 2)
		{
			if(BD[t.reg2] >= 0 && BD[t.reg3] >= 0 && (BD[t.reg2] + BD[t.reg3) < 0) overflow = 1;
			else if(BD[t.reg2] < 0 && BD[t.reg3] < 0 && (BD[t.reg2] + BD[t.reg3]) >= 0)overflow =1;
			else if(BD[t.reg2] < 0 && BD[t.reg3] >= 0 && (BD[t.reg2] -BD[t.reg3]) < 0)overflow = 1;
			else if(BD[t.reg2] >= 0 && BD[t.reg3] < 0 && (BD[t.reg2] - BD[t.reg3]) >= 0)overflow =1;
			else overflow=0;
			if(overflow == 1)carry=1;
			else carry=0;
		}
	}
	
	if(t.opcode == 2 || t.opcode == 5 || t.opcode == 9 || t.opcode == 10 || t.opcode == 14 || t.opcode == 16 || t.opcode == 19 || t.opcode == 22) // R
	{
		if(t.modo == 2)
		{
			if(BD[t.reg3] >= 0 && BD[t.reg4] >= 0 && (BD[t.reg3] + BD[t.reg4]) < 0) overflow = 1;
			else if(BD[t.reg3] < 0 && BD[t.reg4] < 0 && (BD[t.reg3] + BD[t.reg4]) >= 0)overflow =1;
			else if(BD[t.reg3] < 0 && BD[t.reg4] >= 0 && (BD[t.reg3] -BD[t.reg4]) < 0)overflow = 1;
			else if(BD[t.reg3] >= 0 && BD[t.reg4] < 0 && (BD[t.reg3] - BD[t.reg4]) >= 0)overflow =1;
			else overflow=0;
			if(overflow == 1)carry=1;
			else carry=0;
		}
		
	else if(t.opcode == 24 || t.opcode == 25) // inc/dec
	{
		if(t.opcode == 24)
		{
			if(t.modo == 2)
			{
				if(BD[t.reg1] >= 0 && 1 >= 0 && (BD[t.reg1] + 1) < 0) overflow = 1;
				else if(BD[t.reg1] < 0 && 1 < 0 && (BD[t.reg1] + 1) >= 0)overflow =1;
				else if(BD[t.reg1] < 0 && 1 >= 0 && (BD[t.reg1] - 1) < 0)overflow = 1;
				else if(BD[t.reg1] >= 0 && 1 < 0 && (BD[t.reg1] - 1) >= 0)overflow =1;
				else overflow=0;
				if(overflow == 1)carry=1;
				else carry=0;
			}
		}
		else if(t.opcode == 25)
		{
			if(t.modo == 2)
			{
				if(BD[t.reg1] >= 0 && -1 >= 0 && (BD[t.reg1] + -1) < 0) overflow = 1;
				else if(BD[t.reg1] < 0 && -1 < 0 && (BD[t.reg1] + -1) >= 0)overflow =1; 
				else if(BD[t.reg1] < 0 && -1 >= 0 && (BD[t.reg1] - -1) < 0)overflow = 1;
				else if(BD[t.reg1] >= 0 && -1 < 0 && (BD[t.reg1] - -1) >= 0)overflow =1;
				else overflow=0;
				if(overflow == 1)carry=1;
				else carry=0;
			}
		}
	}
	switch(t.formato)
	{
		case(0):  // LA
		{
			switch(t.opcode)
			{
				case(57): // nop
				{
					zero=-1,carry=-1,overflow=-1;
					break;
				}
				case(0): // add2
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] + BD[t.reg2];
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] + BD[t.reg2];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
					}
					break;
				case(1): // add3
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] + BD[t.reg2] + BD[t.reg3];
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] + BD[t.reg2] + BD[t.reg3];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = BD[t.reg1] + BD[t.reg2] + BD[t.reg3];
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;
							}
						}
					}
					break;
				case(2): // add4
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] + BD[t.reg2] + BD[t.reg3] + BD[t.reg4];
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] + BD[t.reg2] + BD[t.reg3] + BD[t.reg4];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = BD[t.reg2] + BD[t.reg3] + BD[t.reg4];
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 4)
							{
								BD[t.reg4] = BD[t.reg1] + BD[t.reg2] + BD[t.reg3] + BD[t.reg4];
								if(BD[t.reg4] == 0)zero = 1;
								else zero = 0;
							}
							break;	
						}
					}
					break;
				case(3): // fadd2
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] + BD[t.reg2]);
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] + BD[t.reg2]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
					}
					break;
				case(4): // fadd3
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] + BD[t.reg2] + BD[t.reg3]);
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] + BD[t.reg2] + BD[t.reg3]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = fabs(BD[t.reg1] + BD[t.reg2] + BD[t.reg3]);
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;
							}
						}
					}
					break;
				case(5): // fadd4
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] + BD[t.reg2] + BD[t.reg3] + BD[t.reg4]);
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] + BD[t.reg2] + BD[t.reg3] + BD[t.reg4]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = fabs(BD[t.reg1]+ BD[t.reg2] + BD[t.reg3] + BD[t.reg4]);
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 4)
							{
								BD[t.reg4] = fabs(BD[t.reg1] + BD[t.reg2] + BD[t.reg3] + BD[t.reg4]);
								if(BD[t.reg4] == 0)zero = 1;
								else zero = 0;
							}
							break;	
						}
					}
					break;
					
				case(6): // sub2
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] - BD[t.reg2];
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] - BD[t.reg2];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
					}
					break;
				case(7): // sub3
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] - BD[t.reg2] - BD[t.reg3];
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] - BD[t.reg2] - BD[t.reg3];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = BD[t.reg1] - BD[t.reg2] - BD[t.reg3];
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;
							}
						}
					}
					break;
				case(8): // sub4
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] - BD[t.reg2] - BD[t.reg3] - BD[t.reg4];
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] - BD[t.reg2] - BD[t.reg3] - BD[t.reg4];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = BD[t.reg1] - BD[t.reg2] - BD[t.reg3] - BD[t.reg4];
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 4)
							{
								BD[t.reg4] = BD[t.reg1] - BD[t.reg2] - BD[t.reg3] - BD[t.reg4];
								if(BD[t.reg4] == 0)zero = 1;
								else zero = 0;
							}
							break;	
						}
					}
					break;
				case(9): // fsub2
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] - BD[t.reg2]);
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] - BD[t.reg2]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
					}
					break;
				case(10): // fsub3
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] - BD[t.reg2] - BD[t.reg3]);
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] - BD[t.reg2] - BD[t.reg3]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = fabs(BD[t.reg1] - BD[t.reg2] - BD[t.reg3]);
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;
							}
						}
					}
					break;
				case(11): // fsub4
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] - BD[t.reg2] - BD[t.reg3] - BD[t.reg4]);
								if(BD[t.reg1] == 0) zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] - BD[t.reg2] - BD[t.reg3] - BD[t.reg4]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = fabs(BD[t.reg1] - BD[t.reg2] - BD[t.reg3] - BD[t.reg4]);
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 4)
							{
								BD[t.reg4] = fabs(BD[t.reg1] - BD[t.reg2] - BD[t.reg3] - BD[t.reg4]);
								if(BD[t.reg4] == 0)zero = 1;
								else zero = 0;
							}
							break;	
						}
					}
					break;
				}
				case(24): //inc1
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 0)
							{
								BD[t.reg0] = BD[t.reg0] + 1;
								if(BD[t.reg0] == 0)zero = 1;
								else zero = 0;	
							}
							else if(t.regdest == 1)
							{
								BD[t.reg0] = BD[t.reg0] + 1;
								if(BD[t.reg0] == 0)zero = 1;
								else zero = 0;		

							}
							break;
						}
					}
					break;
				}
				case(25): //dec1
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 0)
							{
								BD[t.reg0] = BD[t.reg0] - 1;
								if(BD[t.reg0] == 0)zero = 1;
								else zero = 0;
							}
							else if(t.regdest == 1)
							{
								BD[t.reg0] = BD[t.reg0] - 1;
								if(BD[t.reg0] == 0)zero = 1;
								else zero = 0;	
							}
							break;
						}
					}
					break;
				}
				case(12): //mul2
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] * BD[t.reg2];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] * BD[t.reg2];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							break;
						}
					}
					break;
				}
					case(13): //mul3
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] * BD[t.reg2]*BD[t.reg3];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] * BD[t.reg2]*BD[t.reg3];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = BD[t.reg1] * BD[t.reg2]*BD[t.reg3];
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;		

							}
							break;
						}
					}
					break;
				}
				case(14): //mul4
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] * BD[t.reg2]*BD[t.reg3]*BD[t.reg4];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] * BD[t.reg2]*BD[t.reg3]*BD[t.reg4];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = BD[t.reg1] * BD[t.reg2]*BD[t.reg3]*BD[t.reg4];
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 4)
							{
								BD[t.reg4] = BD[t.reg1] * BD[t.reg2]*BD[t.reg3]*BD[t.reg4];
								if(BD[t.reg4] == 0)zero = 1;
								else zero = 0;	
			
							}
							break;
						}
					}
					break;
				}
				case(15): //fmul2
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] * BD[t.reg2]);
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] * BD[t.reg2]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							break;
						}
					}
					break;
				}
					case(16): //fmul3
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] * BD[t.reg2]*BD[t.reg3]);
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] * BD[t.reg2]*BD[t.reg3]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = fabs(BD[t.reg1] * BD[t.reg2]*BD[t.reg3]);
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;		

							}
							break;
						}
					}
					break;
				}
				case(17): //fmul4
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] * BD[t.reg2]*BD[t.reg3]*BD[t.reg4]);
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] * BD[t.reg2]*BD[t.reg3]*BD[t.reg4]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = fabs(BD[t.reg1] * BD[t.reg2]*BD[t.reg3]*BD[t.reg4]);
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 4)
							{
								BD[t.reg4] = fabs(BD[t.reg1] * BD[t.reg2]*BD[t.reg3]*BD[t.reg4]);
								if(BD[t.reg4] == 0)zero = 1;
								else zero = 0;	
			
							}
							break;
						}
					}
					break;
				}
				case(18): //div2
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 0)
							{
								BD[t.reg1] = BD[t.reg1] / BD[t.reg2];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 1)
							{
								BD[t.reg2] = BD[t.reg1] / BD[t.reg2];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							break;
						}
					}
					break;
				}
				case(19): //div3
				{
					switch(t.modo)
					{
						case(2):
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] / BD[t.reg2]/BD[t.reg3];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] / BD[t.reg2]/BD[t.reg3];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = BD[t.reg1] / BD[t.reg2]/BD[t.reg3];
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;		
							}
						}
					}
					break;
				}
				case(20): //div4
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] / BD[t.reg2]/BD[t.reg3]/BD[t.reg4];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] / BD[t.reg2]/BD[t.reg3]/BD[t.reg4];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = BD[t.reg1] / BD[t.reg2]/BD[t.reg3]/BD[t.reg4];
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;		
							}
							else if(t.regdest == 4)
							{
								BD[t.reg4] = BD[t.reg1] / BD[t.reg2]/BD[t.reg3]/BD[t.reg4];
								if(BD[t.reg4] == 0)zero = 1;
								else zero = 0;
							}
						}
					}
					break;
				}
				case(21): //fdiv2
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 0)
							{
								BD[t.reg1] = fabs(BD[t.reg1] / BD[t.reg2]);
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 1)
							{
								BD[t.reg2] = fabs(BD[t.reg1] / BD[t.reg2]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							break;
						}
					}
					break;
				}
				case(19): //div3
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] / BD[t.reg2]/BD[t.reg3]);
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] / BD[t.reg2]/BD[t.reg3]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = fabs(BD[t.reg1] / BD[t.reg2]/BD[t.reg3]);
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;		
							}
						}
					}
					break;
				}
				case(23): //fdiv4
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = fabs(BD[t.reg1] / BD[t.reg2]/BD[t.reg3]/BD[t.reg4]);
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = fabs(BD[t.reg1] / BD[t.reg2]/BD[t.reg3]/BD[t.reg4]);
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 3)
							{
								BD[t.reg3] = fabs(BD[t.reg1] / BD[t.reg2]/BD[t.reg3]/BD[t.reg4]);
								if(BD[t.reg3] == 0)zero = 1;
								else zero = 0;		
							}
							else if(t.regdest == 4)
							{
								BD[t.reg4] = fabs(BD[t.reg1] / BD[t.reg2]/BD[t.reg3]/BD[t.reg4]);
								if(BD[t.reg4] == 0)zero = 1;
								else zero = 0;
							}
						}
					}
					break;
				}
				case(26): //mod2
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] % BD[t.reg2];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		

							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] % BD[t.reg2];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;
							}
							break;
						}
					}
					break;
				}
				case(31): //shr2
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								x = BD[t.reg1] << 32;
								x = x >> 32;
								BD[t.reg1] = BD[t.reg1] >> 1;
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;
								if(x == 1)carry=1;
								else carry=0;
							}
							else if(t.regdest == 2)
							{
								x = BD[t.reg0] << 32;
								x = x >> 32;
								BD[t.reg1] = BD[t.reg1] >> 1;
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;	
								if(x == 1)carry=1;
								else carry=0;
							}
							overflow=-1;
							break;
						}
					}
					break;
				}
				case(32): //shl2
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 0)
							{
								x = BD[t.reg1] >> 15;
								BD[t.reg1] = BD[t.reg1] << 1;
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		
								if(x == 1)carry=1;
								else carry=0;	
							}
							else if(t.regdest == 1)
							{
								x = BD[t.reg1] >> 15;
								BD[t.reg1] = BD[t.reg1] << 1;
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;
								if(x == 1)carry=1;
								else carry=0;	
							}
							overflow=-1;
							break;
						}
					}
					break;
				}
				case(28): // or2
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] | BD[t.reg2];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] | BD[t.reg2];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		
							}
							carry = -1,overflow = -1;
							break;
						}
					}
					break;
			}
			case(27): // and2
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] & BD[t.reg2];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] & BD[t.reg2];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		
							}
							carry = -1,overflow = -1;
							break;
						}
					}
					break;
			}
			case(30): // xor2
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = BD[t.reg1] ^ BD[t.reg2];
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		
							}
							else if(t.regdest == 2)
							{
								BD[t.reg2] = BD[t.reg1] ^ BD[t.reg2];
								if(BD[t.reg2] == 0)zero = 1;
								else zero = 0;		
							}
							carry = -1,overflow = -1;
							break;
						}
					}
					break;
			}
			case(29): // not1
				{
					switch(t.modo)
					{
						case(2): 
						{
							if(t.regdest == 1)
							{
								BD[t.reg1] = ~(BD[t.reg1]);
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		
							}
							else if(t.regdest == 2)
							{
								BD[t.reg1] = ~(BD[t.reg1]);
								if(BD[t.reg1] == 0)zero = 1;
								else zero = 0;		
							}
							carry = -1,overflow = -1;
							break;
						}
					}
					break;
				}
			}
			break;
		}
		case(1): // LS
		{
			switch(t.opcode)
			{
				case(45): // lw1
				{
					x = (unsigned short int) MEM_DATA[BD[t.reg2]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg2]+1];
					BD[t.reg1] = x | y;
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				
				case(46): // lw2
				{
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg1] = x | y;
					
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg2] = x | y;
				
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				
				case(47): // lw3
				{
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg1] = x | y;
					
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg2] = x | y;
					
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg3] = x | y;
				
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				
				case(48): // lw4
				{
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg1] = x | y;
					
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg2] = x | y;
					
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg3] = x | y;
					
					x = (unsigned short int) MEM_DATA[BD[t.reg1]];
					x = x << 8;
					x = x | 0;
					y = (unsigned short int) MEM_DATA[BD[t.reg1]+1];
					BD[t.reg4] = x | y;
				
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(37): // sw1
				{
					x = BD[t.reg2] >> 8;
					y = BD[t.reg2] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg1]] =  x;
					MEM_DATA[BD[t.reg1]+1] =  y;
					carry = -1,zero = -1,overflow = -1;
					break;
					
				}
				case(38): // sw2
				{
					x = BD[t.reg2] >> 8;
					y = BD[t.reg2] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg1]] =  x;
					MEM_DATA[BD[t.reg1]+1] =  y;
	
					x = BD[t.reg1] >> 8;
					y = BD[t.reg1] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg2]] =  x;
					MEM_DATA[BD[t.reg2]+1] =  y;
					carry = -1,zero = -1,overflow = -1;
					break;
					
				}
				case(39): // sw3
				{
					x = BD[t.reg2] >> 8;
					y = BD[t.reg2] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg1]] =  x;
					MEM_DATA[BD[t.reg1]+1] =  y;
					
					x = BD[t.reg1] >> 8;
					y = BD[t.reg1] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg2]] =  x;
					MEM_DATA[BD[t.reg2]+1] =  y;
					
					x = BD[t.reg2] >> 8;
					y = BD[t.reg2] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg3]] =  x;
					MEM_DATA[BD[t.reg3]+1] =  y;
					
					carry = -1,zero = -1,overflow = -1;
					break;
					
				}
				case(40): // sw4
				{
					x = BD[t.reg2] >> 8;
					y = BD[t.reg2] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg1]] =  x;
					MEM_DATA[BD[t.reg1]+1] =  y;
					
					x = BD[t.reg1] >> 8;
					y = BD[t.reg1] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg2]] =  x;
					MEM_DATA[BD[t.reg2]+1] =  y;
					
					x = BD[t.reg2] >> 8;
					y = BD[t.reg2] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg3]] =  x;
					MEM_DATA[BD[t.reg3]+1] =  y;
					
					x = BD[t.reg3] >> 8;
					y = BD[t.reg3] << 8;
					y = y >> 8;
					MEM_DATA[BD[t.reg4]] =  x;
					MEM_DATA[BD[t.reg4]+1] =  y;
					
					carry = -1,zero = -1,overflow = -1;
					break;
					
				}
				case(33): // lb1
				{
					BD[t.reg1] = (unsigned short int)MEM_DATA[BD[t.reg2]];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(34): // lb2
				{
					BD[t.reg1] = (unsigned short int)MEM_DATA[BD[t.reg2]];
					BD[t.reg2] = (unsigned short int)MEM_DATA[BD[t.reg3]];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(35): // lb3
				{
					BD[t.reg1] = (unsigned short int)MEM_DATA[BD[t.reg2]];
					BD[t.reg2] = (unsigned short int)MEM_DATA[BD[t.reg3]];
					BD[t.reg3] = (unsigned short int)MEM_DATA[BD[t.reg4]];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(36): // lb4
				{
					BD[t.reg1] = (unsigned short int)MEM_DATA[BD[t.reg2]];
					BD[t.reg2] = (unsigned short int)MEM_DATA[BD[t.reg3]];
					BD[t.reg3] = (unsigned short int)MEM_DATA[BD[t.reg4]];
					BD[t.reg4] = (unsigned short int)MEM_DATA[BD[t.reg1]];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(41): // sb1
				{
				 	MEM_DATA[BD[t.reg1]] = BD[t.reg2];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(42): // sb2
				{
				 	MEM_DATA[BD[t.reg1]] = BD[t.reg2];
				 	MEM_DATA[BD[t.reg2]] = BD[t.reg3];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(43): // sb3
				{
				 	MEM_DATA[BD[t.reg1]] = BD[t.reg2];
				 	MEM_DATA[BD[t.reg2]] = BD[t.reg3];
				 	MEM_DATA[BD[t.reg3]] = BD[t.reg4];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(44): // sb4
				{
				 	MEM_DATA[BD[t.reg1]] = BD[t.reg2];
				 	MEM_DATA[BD[t.reg2]] = BD[t.reg3];
				 	MEM_DATA[BD[t.reg3]] = BD[t.reg4];
				 	MEM_DATA[BD[t.reg4]] = BD[t.reg1];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(49): // cp1
				{
					BD[t.reg1] = BD[t.reg2];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(50): // cp2
				{
					BD[t.reg1] = BD[t.reg3];
					BD[t.reg2] = BD[t.reg3];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
				case(51): // cp3
				{
					BD[t.reg1] = BD[t.reg4];
					BD[t.reg2] = BD[t.reg4];
					BD[t.reg3] = BD[t.reg4];
					carry = -1,zero = -1,overflow = -1;
					break;
				}
			}	
			break;
		}
		case(2): // Cb
		{
			switch(t.opcode)
			{
				case(16): /* Li */
				{
					if(t.regdest == 1)
					{
						BD[28] = 0;
						BD[28] = t.cost;
					}
					else if(t.regdest == 2)
					{
						BD[29] = 0;
						BD[29] = t.cost;
					}
					else if(t.regdest == 3)
					{
						BD[30] = 0;
						BD[30] = t.cost;
					}
						else if(t.regdest == 4)
					{
						BD[31] = 0;
						BD[31] = t.cost;
					}
					
					carry = -1,zero = -1,overflow = -1;
					break;
				}
			}
			break;
		}
		case(3): // JC e JI
		{
			switch(t.opcode)
			{
				case(53): // beq 
				{
					if(BD[t.reg1] == BD[t.reg2])
					{
						PC = PC + (t.desl);
						overflow = -1;
					}
					break;
				}
				case(24): //bne
				{
					if(BD[t.reg1] != BD[t.reg2])
					{
						PC = PC + (t.desl);
						overflow = -1;
					}
					break;
				}
				case(55): // jpc
				{
					PC = PC + (t.desl);
					break;
				}
				case(56): // jpi
				{
					PC = PC;
					break;
				}
			}
			break;
		}
	}
}
int EXECUTA()
{
	Ula(inst0,1);
}
void INTERFACE_SIMULADOR()
{
	do
	{
		printf("\33[H\33[2J");
		/*
		printf("\n\t\t|| -- 	           ISS MIPS-A      	      -- ||\n");
		printf("\t\t|| --  				    	      -- ||\n");
		printf("\t\t|| --    (E) - Executar numero X de fases    --  ||\n");
		printf("\t\t|| --    (C) - Executar fase a fase          --  ||\n");
		printf("\t\t|| --    (M) - Mostrar ende word memoria dado -- ||\n");
		printf("\t\t|| --    (I) - Mostrar conte end memoria inst -- ||\n");
		printf("\t\t|| --    (Y) - Mostrar inter mem instrucao    -- ||\n");
		printf("\t\t|| --    (D) - Mostrar inter mem dados        -- ||\n");
		printf("\t\t|| --    (B) - Mostrar conteudo BR MI MD      -- ||\n");
		printf("\t\t|| --    (S) - Sair do simulador              -- ||\n");
		printf("\t\t|| --  				    	      -- ||\n");
		printf("\n\t\t\t\t\t\t   Opcao [ ]\b\b");
		fflush(stdin);
		scanf("%c",&opt);
		fflush(stdin);
		*/
		opt = 'C'; 
		opt = toupper(opt);
		if(opt == 'S')
		{
			printf("\33[H\33[2J");
			break;
		}
		switch(opt)
		{
			case('E'): /* Executa um numero de ciclos especificado */
			{
				printf("\nEntre com o numero de fases : [   ]\b\b\b\b");
				scanf("%d",&ciclos);
				printf("\nEntre com o tempo em segundos de cada fase : [  ]\b\b\b");
				scanf("%d",&tempo);
				fflush(stdin);
				do
				{
					for(contador=0;contador<ciclos;contador++)
					{
						fflush(stdin);
						printf("\33[H\33[2J");
						printf("\n||----------------|_-_-_-_-_Execucao da fase_-_-_-_-_|-------------------||\n");
						printf("||----------------|----------------------------------|-------------------||\n");
						printf("||----------------|----------------------------------|-------------------||\n");
						printf("||    Banco Reg   |       Memoria de Instrucao       |      Mem Data     ||\n||\t\t  |\t\t      \t\t     |\t\t         ||\n");
						for(contador2=0;contador2<32;contador2++)
						{
							instrucao0 = MEM_INST[((PC)+contador2)];
							DECODIFICA_INSTRUCAO();
							retorna_instrucao(inst0);
							printf("|| BD[%2d] : %5d | MInst[%2d] : %20s | Mdata[%2d] :",contador2,BD[contador2],((PC)+contador2),sinstrucao,(contador2));
							if(MEM_DATA[(contador2)] != 255)printf(" %5d ||\n",MEM_DATA[(contador2)]);
							else printf("       ||\n");
						}
						printf("||----------------|----------------------------------|-------------------||\n");
						if(flag == 1 || flag == 2)
						{
							printf("\nExecucao encerrada, encerrando simulador !!\n\n");
							sleep(3);
							printf("\33[H\33[2J");
							exit(0);
						}
						instrucao3 = MEM_INST[(PC+3)];
						instrucao2 = MEM_INST[(PC+2)];
						instrucao1 = MEM_INST[(PC+1)];
						instrucao0 = MEM_INST[(PC)];
						x = PC;
						flag = BUSCA_INSTRUCAO();
						inst0 = Decodifica(instrucao0);
						retorna_instrucao(inst0);
						printf("\nInstrucao[%d] : %s\t\t",x,sinstrucao);
						inst1 = Decodifica(instrucao1);
						retorna_instrucao(inst1);
						printf("\n");
						DECODIFICA_INSTRUCAO();
						EXECUTA();
						sleep(tempo);
					}
					printf("\n\nPressione Enter ou qualquer outra tecla para sair !! [ ]\b\b");
					opt1 = getchar();
				}while(opt1 == 10);
				break;
			}
			case('C'): /* Executa ciclo a ciclo */
			{
				do
				{
					fflush(stdin);
					printf("\33[H\33[2J");
					printf("\n||----------------|_-_-_-_Execucao fase a fase_-_-_-_|-------------------||\n");
					printf("||----------------|----------------------------------|-------------------||\n");
					printf("||----------------|----------------------------------|-------------------||\n");
	printf("||    Banco Reg   |       Memoria de Instrucao       |      Mem Data     ||\n||\t\t  |\t\t      \t\t     |\t\t         ||\n");
					for(contador2=0;contador2<32;contador2++)
					{
						instrucao0 = MEM_INST[((PC)+contador2)];
						DECODIFICA_INSTRUCAO();
						retorna_instrucao(inst0);
printf("|| BD[%2d] : %5d | MInst[%2d] : %20s | Mdata[%2d] :",contador2,BD[contador2],((PC)+contador2),sinstrucao,(contador2));
						if(MEM_DATA[(contador2)] != 255)printf(" %5d ||\n",MEM_DATA[(contador2)]);
						else printf("       ||\n");
					}
					printf("||----------------|----------------------------------|-------------------||\n");
					if(flag == 1 || flag == 2)
					{
						printf("\nExecucao encerrada, encerrando simulador !!\n\n");
						sleep(3);
						printf("\33[H\33[2J");
						exit(0);
					}
					instrucao3 = MEM_INST[(PC+3)];
					instrucao2 = MEM_INST[(PC+2)];
					instrucao1 = MEM_INST[(PC+1)];
					instrucao0 = MEM_INST[(PC)];
					x = PC;
					flag = BUSCA_INSTRUCAO();
					inst0 = Decodifica(instrucao0);
					retorna_instrucao(inst0);
					printf("\nInstrucao[%d] : %s\t\t",x,sinstrucao);
					inst1 = Decodifica(instrucao1);
					retorna_instrucao(inst1);
					//printf("Instrucao[%d] : %s\n",x+1,sinstrucao);
					DECODIFICA_INSTRUCAO();
					EXECUTA();
					printf("\n\nPressione uma tecla para continuar !! [ ]\b\b");
					opt1 = getchar();
				}while(opt1 == 10);
				break;
			}
			case('M'): /* Mosta o conteudo de uma word da memoria de dados */
			{
				printf("\nEntre com o endereco da memoria de dados : ");
				scanf("%d",&end_mem);
				fflush(stdin);
				temp0=0,temp1=0;
				temp0 =  MEM_DATA[end_mem];
				temp0 = temp0 << 8;
				temp1 =  MEM_DATA[(end_mem+1)];
				temp1 = temp1 << 8;
				temp1 = temp1 >> 8;
				temp0 = temp0 | temp1;
				if(MEM_DATA[end_mem] == 255 || MEM_DATA[(end_mem+1)] == 255)
				{
					printf("\nEnd Word [%d/%d] :   \n",end_mem,(end_mem+1));
				}
				else
				{
					printf("\nEnd Word [%d/%d] : %xs\n",end_mem,(end_mem+1),temp0);
				}
				printf("\nPressione uma tecla para continuar !! [ ]\b\b");
				getchar();
				break;
			}
			case('I'): /* Mosta o conteudo de um endereco da memoria de instrucoes */
			{
				printf("\nEntre com o endereco da memoria de instrucoes : ");
				scanf("%d",&end_mem);
				fflush(stdin);
				instrucao0 = MEM_INST[end_mem];
				DECODIFICA_INSTRUCAO();
				retorna_instrucao(inst0);
				printf("\nEnd [%d] : %s\n",end_mem,sinstrucao);
				printf("\nPressione uma tecla para continuar !! [ ]\b\b");
				getchar();
				break;
			}
			case('Y'): /* Mostra um intervalo de enderecos da memoria de instrucoes */
			{
				printf("\nEntre com enderedo do inicio do intervalo : ");
				scanf("%d",&ini_inter);
				fflush(stdin);
				printf("\nEntre com endereco do final do intervalo : ");
				scanf("%d",&fim_inter);printf("\n||----------------|_-_-_BD / MEM INST / MEM DATA_-_-_|-------------------||\n");
				printf("||----------------|----------------------------------|-------------------||\n");
	printf("||----------------|----------------------------------|-------------------||\n");
		printf("||    Banco Reg   |       Memoria de Instrucao       |      Mem Data     ||\n||\t\t  |\t\t      \t\t     |\t\t         ||\n");
				for(contador2=0;contador2<16;contador2++)
				{
					instrucao0 = MEM_INST[(PC+contador2)];
					DECODIFICA_INSTRUCAO();
					retorna_instrucao(inst0);
printf("|| BD[%2d] : %5d | MInst[%2d] : %20s | Mdata[%2d] : %5d ||\n",contador2,BD[contador2],(PC+contador2),sinstrucao,contador2,MEM_DATA[contador2]);
				}
				printf("||----------------|----------------------------------|-------------------||\n");
				printf("\nPressione uma tecla para continuar !! [ ]\b\b");
				fflush(stdin);
				printf("\33[H\33[2J");
				printf("\n\n||---_-_-_-_-_Memoria de instrucao_-_-_-_-_---||\n");
				printf("||                                            ||\n");
				for(contador=ini_inter;contador<fim_inter;contador++)
				{
					instrucao0 = MEM_INST[contador];
					DECODIFICA_INSTRUCAO();
					retorna_instrucao(inst0);
					printf("||      End [%3d] : %20s      ||\n",contador,sinstrucao);
				}
				printf("||                                            ||\n");
				printf("\nPressione uma tecla para continuar !! [ ]\b\b");
				getchar();
				break;
			}
			case('D'): /* Mostra um intervalo de enderecos da memoria de dados */
			{
				printf("\nEntre com enderedo do inicio do intervalo : ");
				scanf("%d",&ini_inter);
				fflush(stdin);
				printf("\nEntre com endereco do final do intervalo : ");
				scanf("%d",&fim_inter);
				fflush(stdin);
				printf("\33[H\33[2J");
				printf("\n\n||-----_-_Memoria de dados_-_----||\n");
				printf("||                               ||\n");
				for(contador=ini_inter;contador<fim_inter;contador++)
				{
					printf("||      End [%3d] : %5x        ||\n",contador,MEM_DATA[contador]);
				}
				printf("||                               ||\n");
				printf("\nPressione uma tecla para continuar !! [ ]\b\b");
				getchar();
				break;
			}
			case('B'): /* Mostra o banco de registradores a memoria de instrucoes e a memoria de dados */
			{
				printf("\33[H\33[2J");
				printf("\n||----------------|_-_-_BD / MEM INST / MEM DATA_-_-_|-------------------||\n");
				printf("||----------------|----------------------------------|-------------------||\n");
				printf("||----------------|----------------------------------|-------------------||\n");
printf("||    Banco Reg   |       Memoria de Instrucao       |      Mem Data     ||\n||\t\t  |\t\t      \t\t     |\t\t         ||\n");
				for(contador2=0;contador2<32;contador2++)
				{
					instrucao0 = MEM_INST[(PC+contador2)];
					DECODIFICA_INSTRUCAO();
					retorna_instrucao(inst0);
printf("|| BD[%2d] : %5x | MInst[%2d] : %20s | Mdata[%2d] :",contador2,BD[contador2],(PC+contador2),sinstrucao,contador2);
					if(MEM_DATA[(contador2)] != 255)printf(" %5x ||\n",MEM_DATA[(contador2)]);
					else printf("       ||\n");
				}
				printf("||----------------|----------------------------------|-------------------||\n");
				printf("\nPressione uma tecla para continuar !! [ ]\b\b");
				getchar();
				break;
			}
			default:
			{
				printf("\nOpcao invalida !!\n");
				printf("\nPressione uma tecla para continuar !! [ ]\b\b");
				getchar();
				break;
			}
		}
	}while(1);
}
int main(int argc,char *argv[])
{
	ARQUIVO_ENTRADA(argc,argv[1]); 
	INICIA_SIMULADOR();
	LE_MEMORIA_INSTRUCAO();
	LE_MEMORIA_DADOS();
	INTERFACE_SIMULADOR();
	return(0);
}
